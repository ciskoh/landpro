// ======================== react =========================
import React, { lazy, useEffect, useState } from 'react';
// ======================== styles ========================
import * as S from '../styles/mapPage';
import 'leaflet/dist/leaflet.css';
import "leaflet-draw/dist/leaflet.draw.css";
import "leaflet-geosearch/dist/geosearch.css";
import { Col, Row } from 'antd';
// ===================== translations =====================
import { useTranslation } from 'react-i18next';
// ========================= GIS ==========================
import { FeatureGroup, LayersControl, MapContainer, Polygon } from 'react-leaflet';
import { EditControl } from "react-leaflet-draw";
import { MapLoadSearchSettings, onCreateHandler, onEditHandler, onDeleteHandler } from '../helpers/GIS/drawOnMap'
import MapTiles from '../helpers/GIS/mapTiles';
import DisplayPosition from '../helpers/GIS/displayPosition';


// ====================== components ======================
import { postCoordinates } from '../helpers/fetches';

const Button = lazy(() => import('../components/Button'));
// =========================================================


const MapPage = () => {
  const { t } = useTranslation();

  // map settings
  const [ zoom ] = useState(12);
  const [ position ] = useState([47.0227, 8.303]);
  const [ mapCenter, setMapCenter ] = useState(null);
  
  // go to user location
  const flyTo = () => {
    navigator.geolocation.getCurrentPosition(position => {
        const { latitude, longitude } = position.coords;        
        // console.log(latitude, longitude)
        mapCenter.flyTo([latitude, longitude], 16)
      });
}

  // user marked poylgons
  const [ mapMarkings, setMapMarkings ] = useState([]);
  const [ canSubmit, setCanSubmit ] = useState(false);
  useEffect(() => {
    mapMarkings.length > 0 ? setCanSubmit(true) : setCanSubmit(false);
  },[mapMarkings])
  // server's response
  const [ results, setResults ] = useState({});
  const [ responseCoordinates, setResponseCoordinates ] = useState([])


  
  const colorByVegCO2Estimation = (vegCO2Value) => {
    if (vegCO2Value > 8) return 'darkgreen';
    if (vegCO2Value > 6) return 'lightgreen';
    if (vegCO2Value > 3) return 'darkgoldrod';
    if (vegCO2Value > 0) return 'yellow';
    if (vegCO2Value === 0) return 'lightgray';
  }
  
  const prepareResponseDataAndAddToShowList = (data) => {
    // console.log('data from the start of prepareData: ', data);
    let inverted = [];
    const flipTheCoor = (coorArray) => {
      // console.log('coorArray: ', coorArray)
      const tempArray = [];
      coorArray.forEach(set => {
        const tempSet = [];
        set.forEach((pair) => {
          // console.log('pair: ', pair, "index", index)
          tempSet.push([pair[1], pair[0]]);
        })
        tempArray.push(tempSet);
      })
      // console.log('tempArray: ', tempArray)
      return tempArray;
    }
    data.data.coordinates.forEach((set, index) =>{ 
      // console.log(set)

      const tempFeature = { 
        id: `${data.id}/${index + 1}`, 
        polygonName: colorByVegCO2Estimation(data.data.veg_co_estimates[index]), 
        landCover: colorByVegCO2Estimation(data.data.veg_co_estimates[index]),
        coordinates: flipTheCoor(set)
      };
      // console.log('tempFeature: ', tempFeature);
      inverted.push(tempFeature);
    });
    console.log('corrected array: ', inverted)
    setResponseCoordinates(inverted);
  };


  const handleSubmit = (event) => {
    event.preventDefault();
    // console.log(mapMarkings);
    mapMarkings.map(markings =>
      postCoordinates(markings)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setResults(data);
        prepareResponseDataAndAddToShowList(data);
      })
    )
  };


  
  return (
      <>  

        <Row type='flex' justify='space-around' align='middle'>
          <Col lg={4} md={6} sm={8} xs={6}>
            <Button onClick={ flyTo }>{ t('demo.mapPage.toUser') }</Button>
          </Col>
          <Col lg={8} md={6} sm={8} xs={6}>
            <S.SubmitPolygons active={ canSubmit } onClick={ handleSubmit }>
              { t(`demo.mapPage.${ mapMarkings.length === 0 ? 'submitNotActive' : mapMarkings.length === 1 ? 'submitPolygon' : 'submitPolygons' }`) }
            </S.SubmitPolygons>
          </Col>
          <Col lg={10} md={8} sm={0} xs={0}>
            { 
              mapCenter && <DisplayPosition map={ mapCenter } center={ position } zoom={ zoom } /> 
            }
          </Col>
        </Row>
          <S.MapWrapper>
            <MapContainer id='map' center={ position } zoom={ zoom } whenCreated={ setMapCenter } scrollWheelZoom={ false }>
              <MapLoadSearchSettings />
              <FeatureGroup> 
                <EditControl
                  position="topright"
                  onCreated={ (event) => onCreateHandler(event, setMapMarkings) }
                  onEdited={ (event) => onEditHandler(event, setMapMarkings) }
                  onDeleted={ (event) => onDeleteHandler(event, setMapMarkings) }
                  draw={{ rectangle: false, polyline: false, circle: false, circlemarker: false, marker: false }}
                  />
              </FeatureGroup>  
              <LayersControl position="topleft">

                <MapTiles/>
              { responseCoordinates.length && responseCoordinates.map((feature) => 
                  <LayersControl.Overlay key={feature.id} name={`${feature.polygonName}`}>
                        <Polygon positions={ feature.coordinates } color={ feature.landCover }/>
                  </LayersControl.Overlay>
                )
              }
              </LayersControl>

            </MapContainer>
          </S.MapWrapper>
          { mapMarkings.length > 0 && <p>{JSON.stringify(mapMarkings.length)}</p>

           }
          { 
            Object.entries(results).length > 0 && 
            <>
            <p> 
              <strong>original polygon:</strong>{results.coordinates.features[0].geometry.coordinates[0].map(item => <li key={ item }>{JSON.stringify([item[1].toFixed(4),item[0].toFixed(4)])}</li>)}
            </p> 
              <br/>
              
              <strong>response multipolygon:</strong>
              {
                Object.entries(results.data.coordinates).map((item, index) => 
                <li key={ index }>{ item[1][0].map(coor => 
                    <p key={ `${index}-${coor}${Math.random()}` }>{JSON.stringify([coor[1],coor[0]])}</p>)}
                </li>)
              }
              
            <p>
              <strong>Veg co estimates:</strong> {<li>{JSON.stringify(Object.entries(results.data.veg_co_estimates))}</li>}
              <strong>Soil co estimates:</strong>{<li>{JSON.stringify(Object.entries(results.data.soil_co_estimates))}</li>}
            </p> 
            </>
          }
    </>
  );
};

export default MapPage;
