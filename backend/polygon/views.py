from rest_framework.generics import ListAPIView, CreateAPIView

from calculations.predict_main import predict_main
from polygon.models import Polygon
from polygon.serializers import PolygonSerializer


class ViewPolygons(ListAPIView):
    queryset = Polygon.objects.all()
    serializer_class = PolygonSerializer
    permission_classes = []


class NewCoordinatesPolygons(CreateAPIView):
    queryset = Polygon.objects.all()
    serializer_class = PolygonSerializer
    permission_classes = []

    def perform_create(self, serializer):
        data = self.request.data
        print("data: ", data)
        coor = data.get("geoJSON")
        print("coor: ", coor)
        serializer.save(coordinates=coor)

        predict_results = predict_main(coor)
        print('predict)results: ', predict_results)

        serializer.save(data=predict_results)
