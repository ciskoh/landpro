from django.db import models


class Polygon(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    coordinates = models.JSONField(default=dict, blank=True, null=True)
    data = models.JSONField(default=dict, blank=True, null=True)

    def __str__(self):
        return f'{self.pk}: {self.coordinates}'
