# Generated by Django 3.2 on 2021-06-23 20:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polygon', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='polygon',
            name='coordinates',
            field=models.JSONField(blank=True, default=dict, null=True),
        ),
        migrations.AlterField(
            model_name='polygon',
            name='data',
            field=models.JSONField(blank=True, default=dict, null=True),
        ),
    ]
